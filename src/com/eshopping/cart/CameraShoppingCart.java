package com.eshopping.cart;

import java.util.ArrayList;
import java.util.HashMap;

import com.eshopping.items.Product;

public class CameraShoppingCart {

	ArrayList<Integer> productCart;
	
	
	public CameraShoppingCart(){
		productCart = new ArrayList<Integer>();
	}
	public void addToCart(int product_ID){
		if(!productCart.contains(product_ID))
		{
			productCart.add(product_ID);
		}
		
	}
	
	public void clearCart(){
		productCart.clear();
	}

	public ArrayList<Integer> getItems(){
		return productCart;
	}
	
}


