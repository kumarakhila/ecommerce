package com.eshopping.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.eshopping.items.Dimension;
import com.eshopping.items.General;
import com.eshopping.items.Image;
import com.eshopping.items.Lens;
import com.eshopping.items.Pixel;
import com.eshopping.items.Product;
import com.eshopping.items.ProductDetails;
import com.eshopping.items.Sensor;
import com.eshopping.items.Shutter;
import com.eshopping.items.Reviews;

public class ProductDBAO {

	Connection con;
	public static String url = "jdbc:mysql://localhost:3306/products";
	public static String dbdriver = "com.mysql.jdbc.Driver";
	public static String username = "root";
	public static String password = "root";
	PreparedStatement allProducts;
	PreparedStatement brandFilter;
	PreparedStatement dimensionDetail;
	PreparedStatement generalDetail;
	PreparedStatement imageDetail;
	PreparedStatement lensDetail;
	PreparedStatement pixelDetail;
	PreparedStatement productDetail;
	PreparedStatement sensornDetail;
	PreparedStatement shutterDetail;
	PreparedStatement compare;
	PreparedStatement productBasicInfo;
	PreparedStatement reviewsDetail;

	private static final String SELECT_PRODUCTBASICINFO = "SELECT * FROM PRODUCT_INFO WHERE Product_ID = ?";
	private static final String SELECT_ALL_PRODUCTS = "SELECT * FROM PRODUCT_INFO";
	private static final String SELECT_BRAND_FILTER = "SELECT * FROM PRODUCT_INFO WHERE BRAND = ?";
	private static final String SELECT_PRODUCT_DIMENSION_DETAILS = "SELECT * FROM DIMENSION_INFO WHERE Product_ID = ?";
	private static final String SELECT_PRODUCT_GENERAL_DETAILS = "SELECT * FROM GENERAL_INFO WHERE Product_ID = ?";
	private static final String SELECT_PRODUCT_IMAGE_DETAILS = "SELECT * FROM IMAGE_INFO WHERE Product_ID = ?";
	private static final String SELECT_PRODUCT_LENS_DETAILS = "SELECT * FROM LENS_INFO WHERE Product_ID= ?";
	private static final String SELECT_PRODUCT_PIXEL_DETAILS = "SELECT * FROM PIXEL_INFO WHERE Product_ID = ?";
	private static final String SELECT_PRODUCT_DETAILS = "SELECT * FROM PRODUCT_INFO WHERE Product_ID = ?";
	private static final String SELECT_PRODUCT_SENSOR_DETAILS = "SELECT * FROM SENSOR_INFO WHERE Product_ID = ?";
	private static final String SELECT_PRODUCT_SHUTTER_DETAILS = "SELECT * FROM SHUTTER_INFO WHERE Product_ID = ?";
	private static final String SELECT_TO_COMPARE = "SELECT PRODUCT_ID,PRODUCT_NAME FROM PRODUCT_INFO";
	private static final String SELECT_REVIEWS_DETAILS = "SELECT * FROM REVIEWS_INFO WHERE Product_ID = ?";

	public ProductDBAO() throws Exception {

		try {
			Class.forName(dbdriver);
			con = DriverManager.getConnection(url, username, password);
			allProducts = con.prepareStatement(SELECT_ALL_PRODUCTS);
			brandFilter = con.prepareStatement(SELECT_BRAND_FILTER);
			dimensionDetail = con
					.prepareStatement(SELECT_PRODUCT_DIMENSION_DETAILS);
			generalDetail = con
					.prepareStatement(SELECT_PRODUCT_GENERAL_DETAILS);
			imageDetail = con.prepareStatement(SELECT_PRODUCT_IMAGE_DETAILS);
			lensDetail = con.prepareStatement(SELECT_PRODUCT_LENS_DETAILS);
			pixelDetail = con.prepareStatement(SELECT_PRODUCT_PIXEL_DETAILS);
			productDetail = con.prepareStatement(SELECT_PRODUCT_DETAILS);
			sensornDetail = con.prepareStatement(SELECT_PRODUCT_SENSOR_DETAILS);
			shutterDetail = con
					.prepareStatement(SELECT_PRODUCT_SHUTTER_DETAILS);
			compare = con.prepareStatement(SELECT_TO_COMPARE);
			reviewsDetail = con.prepareStatement(SELECT_REVIEWS_DETAILS);
			productBasicInfo = con.prepareStatement(SELECT_PRODUCTBASICINFO);

		}

		catch (Exception e) {
			System.out.println("Exception in ProductDBAO: " + e);
			throw new Exception("Couldn't open connection to database: "
					+ e.getMessage());

		}

	}

	public Product getProductBasicInfo(int product_ID) {
		Product productObj=null;
		
		try {
			productBasicInfo.setInt(1, product_ID);
			ResultSet result = productBasicInfo.executeQuery();
			result.next();
			productObj = new Product(result.getInt(1), result.getString(2),
					result.getString(3), result.getInt(4), result.getString(5),
					result.getString(6), result.getInt(7));

		} catch (Exception e) {
			System.out.println(e);

		}

		return productObj;
	}

	public ArrayList<Product> getAllProducts() {
		ArrayList<Product> products = new ArrayList<Product>();
		try {
			ResultSet result = allProducts.executeQuery();
			while (result.next()) {
				Product productObj = new Product(result.getInt(1),
						result.getString(2), result.getString(3),
						result.getInt(4), result.getString(5),
						result.getString(6), result.getInt(7));
				products.add(productObj);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return products;
	}

	public ArrayList<Product> getBrandFilter(String brand) {
		ArrayList<Product> products = new ArrayList<Product>();
		try {
			brandFilter.setString(1, brand);
			ResultSet result = brandFilter.executeQuery();
			while (result.next()) {
				Product productObj = new Product(result.getInt(1),
						result.getString(2), result.getString(3),
						result.getInt(4), result.getString(5),
						result.getString(6), result.getInt(7));
				products.add(productObj);
				;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return products;

	}

	public ArrayList<Product> compare() {
		ArrayList<Product> products = new ArrayList<Product>();
		try {
			ResultSet result = compare.executeQuery();
			while (result.next()) {
				Product productObj = new Product(result.getInt(1),
						result.getString(2), result.getString(3),
						result.getInt(4), result.getString(5),
						result.getString(6), result.getInt(7));
				products.add(productObj);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return products;
	}

	public ProductDetails getProductDetails(int productID) {
		ProductDetails productDetails = new ProductDetails();
		Dimension dimension;
		General general;
		Image image;
		Lens lens;
		Pixel pixel;
		Product product;
		Sensor sensor;
		Shutter shutter;
		Reviews reviews;

		try {
			dimensionDetail.setInt(1, productID);
			ResultSet result = dimensionDetail.executeQuery();
			result.first();
			dimension = new Dimension(result.getInt(1), result.getString(2),
					result.getString(3));

			generalDetail.setInt(1, productID);
			result = generalDetail.executeQuery();
			result.first();
			general = new General(result.getInt(1), result.getString(2),
					result.getString(3), result.getString(4));

			imageDetail.setInt(1, productID);
			result = imageDetail.executeQuery();
			result.first();
			image = new Image(result.getInt(1), result.getString(2),
					result.getString(3), result.getString(4),
					result.getString(5), result.getString(6));

			lensDetail.setInt(1, productID);
			result = lensDetail.executeQuery();
			result.first();
			lens = new Lens(result.getInt(1), result.getString(2),
					result.getString(3), result.getString(4),
					result.getString(5));

			pixelDetail.setInt(1, productID);
			result = pixelDetail.executeQuery();
			result.first();
			pixel = new Pixel(result.getInt(1), result.getString(2));

			productDetail.setInt(1, productID);
			result = productDetail.executeQuery();
			result.first();
			product = new Product(result.getInt(1), result.getString(2),
					result.getString(3), result.getInt(4), result.getString(5),
					result.getString(6), result.getInt(7));

			sensornDetail.setInt(1, productID);
			result = sensornDetail.executeQuery();
			result.first();
			sensor = new Sensor(result.getInt(1), result.getString(2),
					result.getString(3));

			shutterDetail.setInt(1, productID);
			result = shutterDetail.executeQuery();
			result.first();
			shutter = new Shutter(result.getInt(1), result.getString(2),
					result.getString(3));

			reviewsDetail.setInt(1, productID);
			result = reviewsDetail.executeQuery();
			result.first();
			reviews = new Reviews(result.getInt(1), result.getString(2),
					result.getString(3), result.getString(4),
					result.getString(5), result.getString(6));

			productDetails = new ProductDetails(dimension, general, image,
					lens, pixel, product, sensor, shutter, reviews);

		} catch (Exception e) {

			System.out.println(e);
		}

		return productDetails;

	}
}
