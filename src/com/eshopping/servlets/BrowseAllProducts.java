package com.eshopping.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eshopping.database.ProductDBAO;
import com.eshopping.items.Product;
import com.google.gson.Gson;

/**
 * Servlet implementation class BrowseAllProducts
 */
@WebServlet("/BrowseAllProducts")
public class BrowseAllProducts extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public BrowseAllProducts() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			String selection=null;
			ProductDBAO products = new ProductDBAO();
			ArrayList<Product> productList = null;
			selection = request.getParameter("Product");
			
			if (selection.equals("all")){
				productList = products.getAllProducts();	
			}
			else{
				
				productList = products.getBrandFilter(selection);
			}
			
			Gson gson = new Gson();
			String json = gson.toJson(productList);
			response.setContentType("text/html");
			PrintWriter out= response.getWriter();
			out.print(json);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
