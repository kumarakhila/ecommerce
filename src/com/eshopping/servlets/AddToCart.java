package com.eshopping.servlets;

import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eshopping.cart.CameraShoppingCart;

/**
 * Servlet implementation class AddToCart
 */
@WebServlet("/AddToCart")
public class AddToCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddToCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int productID = Integer.parseInt(request.getParameter("product_ID"));
		HttpSession session =request.getSession();
		CameraShoppingCart cart = (CameraShoppingCart)session.getAttribute("cart");
		
		if(cart == null){
			 cart = new CameraShoppingCart();
		}
		
		cart.addToCart(productID);
		session.setAttribute("cart", cart);
		System.out.println("No of items in cart"+cart.getItems().size());
//		response.sendRedirect(request.getContextPath()+"/AddToCart.jsp");
		//getServletContext()
   //             .getRequestDispatcher("/ConfirmAdd.jsp").include(request, response);
//        requestDispatcher.forward(request, response);
		//return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
