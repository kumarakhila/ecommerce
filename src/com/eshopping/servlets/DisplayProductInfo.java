package com.eshopping.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eshopping.database.ProductDBAO;
import com.eshopping.items.Product;
import com.eshopping.items.ProductDetails;
import com.google.gson.Gson;

/**
 * Servlet implementation class DisplayProductInfo
 */
@WebServlet("/DisplayProductInfo")
public class DisplayProductInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayProductInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			String productString = request.getParameter("productID");
			int productID=Integer.parseInt(productString);
		
			
			ProductDBAO products = new ProductDBAO();
			ProductDetails productList = products.getProductDetails(productID);
			Gson gson = new Gson();
			String json = gson.toJson(productList);
			response.setContentType("text/html");
			PrintWriter out= response.getWriter();
			out.print(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
