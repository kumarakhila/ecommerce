package com.eshopping.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eshopping.cart.CameraShoppingCart;
import com.eshopping.database.ProductDBAO;
import com.eshopping.items.Product;
import com.google.gson.Gson;

/**
 * Servlet implementation class DisplayCart
 */
@WebServlet("/DisplayCart")
public class DisplayCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String clear=request.getParameter("clear");
		int shouldClear=0;
		if(clear!=null){
			shouldClear=Integer.parseInt(clear);
		}
		CameraShoppingCart cart = (CameraShoppingCart)request.getSession(false).getAttribute("cart");
		ArrayList<Integer> prodArray = cart.getItems();
		System.out.print(prodArray.size());
		ArrayList<Product> productList = new ArrayList<Product> ();
		for(Integer productID : prodArray){
			try {
				ProductDBAO db = new ProductDBAO();
				productList.add(db.getProductBasicInfo(productID));
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Gson gsonObj = new Gson();
		String json = gsonObj.toJson(productList);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		System.out.print(json);
		if(shouldClear==1){
			cart.clearCart();
		}
		out.print(json);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
