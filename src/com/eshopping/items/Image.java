package com.eshopping.items;

public class Image {

	int productID;
	String imageSizing;
	String videoFormat;
	String imageFormat;
	String faceDetection;
	String shottingModes;

	public Image() {

	}

	public Image(int productID, String imageSizing, String videoFormat,
			String imageFormat, String faceDetection, String shottingModes) {
		super();
		this.productID = productID;
		this.imageSizing = imageSizing;
		this.videoFormat = videoFormat;
		this.imageFormat = imageFormat;
		this.faceDetection = faceDetection;
		this.shottingModes = shottingModes;
	}

	public int getProductID() {
		return productID;
	}

	public String getImageSizing() {
		return imageSizing;
	}

	public String getVideoFormat() {
		return videoFormat;
	}

	public String getImageFormat() {
		return imageFormat;
	}

	public String getFaceDetection() {
		return faceDetection;
	}

	public String getShottingModes() {
		return shottingModes;
	}

}
