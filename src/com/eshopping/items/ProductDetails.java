package com.eshopping.items;

public class ProductDetails {
	
	Dimension dimension;
	General general;
	Image image;
	Lens lens;
	Pixel pixel;
	Product product;
	Sensor sensor;
	Shutter shutter;
	Reviews reviews;
	
	public ProductDetails(){
		
	}
	
	public ProductDetails(Dimension dimension, General general, Image image,
			Lens lens, Pixel pixel, Product product, Sensor sensor,
			Shutter shutter, Reviews reviews) {
		super();
		this.dimension = dimension;
		this.general = general;
		this.image = image;
		this.lens = lens;
		this.pixel = pixel;
		this.product = product;
		this.sensor = sensor;
		this.shutter = shutter;
		this.reviews = reviews;
	}
	public Dimension getDimension() {
		return dimension;
	}
	public General getGeneral() {
		return general;
	}
	public Image getImage() {
		return image;
	}
	public Lens getLens() {
		return lens;
	}
	public Pixel getPixel() {
		return pixel;
	}
	public Product getProduct() {
		return product;
	}
	public Sensor getSensor() {
		return sensor;
	}
	public Shutter getShutter() {
		return shutter;
	}
	public Reviews getReviews() {
		return reviews;
	}
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	public void setGeneral(General general) {
		this.general = general;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	public void setLens(Lens lens) {
		this.lens = lens;
	}
	public void setPixel(Pixel pixel) {
		this.pixel = pixel;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}
	public void setShutter(Shutter shutter) {
		this.shutter = shutter;
	}
	public void setReviews(Reviews reviews) {
		this.reviews = reviews;
	}
	
}
