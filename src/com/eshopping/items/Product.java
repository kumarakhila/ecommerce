package com.eshopping.items;

public class Product {
	int productID;
	String productName;
	String brand;
	int price;
	String imageLink;
	String rating;
	int quantity;
	
	public Product()
	{
		
	}

	public Product(int productID, String productName, String brand, int price,
			String imageLink, String rating, int quantity) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.brand = brand;
		this.price = price;
		this.imageLink = imageLink;
		this.rating = rating;
		this.quantity = quantity;
	}

	public int getProductID() {
		return productID;
	}

	public String getProductName() {
		return productName;
	}

	public String getBrand() {
		return brand;
	}

	public int getPrice() {
		return price;
	}

	public String getImageLink() {
		return imageLink;
	}

	public String getRating() {
		return rating;
	}

	public int getQuantity() {
		return quantity;
	}
	
	
	
}
