package com.eshopping.items;

public class Sensor {
	int productID;
	String sensorType;
	String sensorSize;
	
	public Sensor()
	{
		
	}

	public Sensor(int productID, String sensorType, String sensorSize) {
		super();
		this.productID = productID;
		this.sensorType = sensorType;
		this.sensorSize = sensorSize;
	}

	public int getProductID() {
		return productID;
	}

	public String getSensorType() {
		return sensorType;
	}

	public String getSensorSize() {
		return sensorSize;
	}
	
	
}
