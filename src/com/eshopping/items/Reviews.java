package com.eshopping.items;

public class Reviews {
	int productID;
	String User1;
	String User2;
	String review1;
	String review2;
	String rating1;

	public Reviews() {

	}

	public Reviews(int productID, String User1, String User2, String review1, String review2, String rating1) {
		this.productID = productID;
		this.User1 = User1;
		this.User2 = User2;
		this.review1 = review1;
		this.review2 = review2;
		this.rating1=rating1;
	}

	public int getProductID() {
		return productID;
	}

	public String getUser1() {
		return User1;
	}

	public String getUser2() {
		return User2;
	}

	public String getReview1() {
		return review1;
	}
	public String getReview2() {
		return review2;
	}
	public String getRating() {
		return rating1;
	}
}
