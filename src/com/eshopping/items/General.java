package com.eshopping.items;

public class General {
	int productID;
	String type;
	String color;
	String modelID;

	public General() {

	}

	public General(int productID, String type, String color, String modelID) {
		this.productID = productID;
		this.type = type;
		this.color = color;
		this.modelID = modelID;
	}

	public int getProductID() {
		return productID;
	}

	public String getType() {
		return type;
	}

	public String getColor() {
		return color;
	}

	public String getModelID() {
		return modelID;
	}

}
