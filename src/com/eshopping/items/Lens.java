package com.eshopping.items;

public class Lens {
	int productID;
	String autoFocus;
	String lensType;
	String manualFocus;
	String focalLength;
	
	public Lens()
	{
		
	}

	public Lens(int productID, String autoFocus, String lensType,
			String manualFocus, String focalLength) {
		super();
		this.productID = productID;
		this.autoFocus = autoFocus;
		this.lensType = lensType;
		this.manualFocus = manualFocus;
		this.focalLength = focalLength;
	}

	public int getProductID() {
		return productID;
	}

	public String getAutoFocus() {
		return autoFocus;
	}

	public String getLensType() {
		return lensType;
	}

	public String getManualFocus() {
		return manualFocus;
	}

	public String getFocalLength() {
		return focalLength;
	}

}
