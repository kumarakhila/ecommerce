package com.eshopping.items;

public class Dimension {
	int productID=0;
	String weight=null;
	String dimension=null;
	
	public Dimension()
	{
	
	}
	
	public Dimension(int productID, String weight, String dimension)
	{
		this.productID=productID;
		this.weight=weight;
		this.dimension=dimension;
	}
	public int getProductID()
	{
		return this.productID;
	}
	public String getWeight()
	{
		return this.weight;
	}
	public String getDimension()
	{
		return this.dimension;
	}
	

}
