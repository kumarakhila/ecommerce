package com.eshopping.items;

public class Pixel {

	int productID;
	String pixel;
	
	public Pixel()
	{
		
	}

	public Pixel(int productID, String pixel) {
		super();
		this.productID = productID;
		this.pixel = pixel;
	}

	public int getProductID() {
		return productID;
	}

	public String getPixel() {
		return pixel;
	}
	
	
}
