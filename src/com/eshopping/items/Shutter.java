package com.eshopping.items;

public class Shutter {
	int productID;
	String maxShutterSpeed;
	String minShutterSpeed;
	
	public Shutter()
	{
		
	}

	public Shutter(int productID, String maxShutterSpeed, String minShutterSpeed) {
		super();
		this.productID = productID;
		this.maxShutterSpeed = maxShutterSpeed;
		this.minShutterSpeed = minShutterSpeed;
	}

	public int getProductID() {
		return productID;
	}

	public String getMaxShutterSpeed() {
		return maxShutterSpeed;
	}

	public String getMinShutterSpeed() {
		return minShutterSpeed;
	}

	

}
