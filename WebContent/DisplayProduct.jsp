<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta property="og:site_name" content="eShopping" />
        <meta property="og:type" content="website">
        <meta property="fb:admins" content="100005171587722">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
        <style type="text/css">
            #addToCart{
                margin-bottom: 15px;
                margin-right: 20px;
            }

            #infoTable{
                margin-left:50px;
                width: 900px;
            }

        </style>
        <script>
            $(document).ready(function() {
                var queryString = window.location.search;
                queryString = queryString.substring(1);
                var params = queryString.split("=");
 
                $.get("DisplayProductInfo/?productID=" + params[1], function(data, status) {

                    var productObj = $.parseJSON(data);
                    console.log(productObj.general.type);

                    var image = $("<img></img>").attr('src', productObj.product.imageLink).css({"height": "300px", "width": "400px", "margin-top": "200px"});
                    var productName = $("<h2></h2>").text(productObj.product.productName).css({"align": "right", "font": "Raleway", "font-size": "20px", "margin-top": "100px"});
                    var productPrice = $("<h3></h3>").text("Price: " + productObj.product.price + "$").css({"font-size": "20px", "font": "Raleway"});
                    var rate = $("<p></p>").text("Rating:").css({"margin-left": "300px", "font-size": "22px", "font": "Raleway"});
                    var productrate = $("<img></img>").attr('src', productObj.product.rating).css({"height": "30px", "width": "85px", "margin-top": "75px", "margin-left": "10px"});
                    var table = $("<table></table>").attr({class: "pure-table pure-table-bordered", id: 'infoTable'});
                    var addToCart = $('<button></button>').attr({type: 'button', id: 'addToCart', name: 'addToCart', value: 'Add To Cart', class: 'pure-button  pure-button-primary'}).css({"font-size": "20px", "font": "Raleway", "height": "50px", "width": "200px", "margin-left": "50px", "margin-top": "50px"});
                    addToCart.text("Add To Cart");
                    //var addToCompare= $('<button></button>').attr({ type: 'button', name:'addToCompare', value:'Add To Compare'}).css({"font-size":"20px","font":"Raleway", "height":"50px","width": "200px", "margin-left":"50px"});
                    //addToCompare.text("Add To Compare");
                    table.append('<thead><tr><th>' + "Dimension" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Weight " + '</td><td>' + productObj.dimension.weight + '</td></tr>' + '<tr><td>' + "Dimension " + '</td><td>' + productObj.dimension.dimension + '</td></tr></tbody>');
                    table.append('<thead><tr><th>' + "General" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Type " + '</td><td>' + productObj.general.type + '</td></tr>' + '<tr><td>' + "Color " + '</td><td>' + productObj.general.color + '</td></tr>' + '<tr><td>' + "Model ID " + '</td><td>' + productObj.general.modelID + '</td></tr></tbody>');
                    table.append('<thead><tr><th>' + "Image" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Image Sizing " + '</td><td>' + productObj.image.imageSizing + '</td></tr>' + '<tr><td>' + "Video Format " + '</td><td>' + productObj.image.videoFormat + '</td></tr>' + '<tr><td>' + "Image Format " + '</td><td>' + productObj.image.imageFormat + '</td></tr>' + '<tr><td>' + "Face Detection " + '</td><td>' + productObj.image.faceDetection + '</td></tr>' + '<tr><td>' + "Shooting Modes " + '</td><td>' + productObj.image.shottingModes + '</td></tr></tbody>');
                    table.append('<thead><tr><th>' + "Lens" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Auto Focus " + '</td><td>' + productObj.lens.autoFocus + '</td></tr>' + '<tr><td>' + "Lens Type " + '</td><td>' + productObj.lens.lensType + '</td></tr>' + '<tr><td>' + "Manual Focus " + '</td><td>' + productObj.lens.manualFocus + '</td></tr>' + '<tr><td>' + "Focal Length " + '</td><td>' + productObj.lens.focalLength + '</td></tr></tbody>');
                    table.append('<thead><tr><th>' + "Pixel" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Pixel " + '</td><td>' + productObj.pixel.pixel + '</td></tr></tbody>');
                    table.append('<thead><tr><th>' + "Sensor" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Sensor Type " + '</td><td>' + productObj.sensor.sensorType + '</td></tr>' + '<tr><td>' + "Sensor Size " + '</td><td>' + productObj.sensor.sensorSize + '</td></tr></tbody>');
                    table.append('<thead><tr><th>' + "Shutter" + '</th><th></th></tr></thead>' + '<tbody><tr><td>' + "Maximum Shutter Speed " + '</td><td>' + productObj.shutter.maxShutterSpeed + '</td></tr>' + '<tr><td>' + "Minimum Shutter Speed " + '</td><td>' + productObj.shutter.minShutterSpeed + '</td></tr></tbody>');
                    var comment = $('<button></button>').attr({id: 'commentbutton', class: 'pure-button', type: 'button', name: 'comments', value: 'Write a comment'}).css({"font-size": "20px", "font": "Raleway", "align": "right", "margin-left": "1145px", "height": "50px", "width": "200px", "margin-top": "50px"});
                    comment.text("Write a comment");
                    comment.click(function() {
                        window.location.assign("comment.jsp");
                    });
                    var header = $("<br><br><div></div>");
                    header.append('<h1>' + "Reviews" + '</h1>');
                    var review1 = $("<div></div>").attr("id", "data1");
                    review1.append('<p>' + productObj.reviews.review1 + '</p>');
                    var user1 = $("<div></div>").attr("id", "user1");
                    user1.append('<p><u>' + productObj.reviews.User1 + '</u></p>');
                    user1.append('<img src="http://4.bp.blogspot.com/-WlG0zlPeh5k/TyhUkdGuUhI/AAAAAAAAP0A/QM2dx6gzAg0/s1600/four-stars.png" width="95" height="30"></img>');
                    var review2 = $("<div></div>").attr("id", "data2");
                    review2.append('<p>' + productObj.reviews.review2 + '</p>');
                    var user2 = $("<div></div>").attr("id", "user2");
                    user2.append('<p><u>' + productObj.reviews.User2 + '</u></p>');
                    user2.append('<img src="http://www.thebooksshereads.com/wp-content/uploads/2013/07/3star.png" width="95" height="30"></img>');
                    var footer = $("<div></div>").attr("id", "footer");
                    footer.append('<p>' + "@ Camera world.com" + '</p>');

                    // Facebook comments
                    var fbcomment = $('<div class="fb-comments" data-href="<%=request.getRequestURL().toString() + "?" + request.getQueryString()%>" data-numposts="5" data-colorscheme="light"></div>');

                    // Meta tag for facebook share
                    var metaOgTitle = document.createElement('meta');
                    metaOgTitle.setAttribute("property", "og:title");
                    metaOgTitle.content = productObj.product.productName;
                    document.getElementsByTagName('head')[0].appendChild(metaOgTitle);

                    var metaOgDesc = document.createElement('meta');
                    metaOgTitle.setAttribute("property", "og:description");
                    metaOgDesc.content = productObj.product.imageLink;
                    document.getElementsByTagName('head')[0].appendChild(metaOgDesc);

                    var metaOgImage = document.createElement('meta');
                    metaOgTitle.setAttribute("property", "og:image");
                    metaOgImage.content = productObj.product.productName + " $" + productObj.product.price;
                    document.getElementsByTagName('head')[0].appendChild(metaOgImage);

                    $('#imageDetails').append(image);
                    $("#productDetails").append(productName, productPrice, rate, addToCart);
                    rate.append(productrate);
                    $("#productDetails").append(table);
                    $("#reviews").append(comment);
                    $("#reviews").append(header, review1, user1, review2, user2);
                    $('#fb-root').append(fbcomment, footer);

                    $("#addToCart").click(function() {
                        $.get("AddToCart/?product_ID=" + params[1], function(data, status) {
                            window.location = "ConfirmAdd.jsp";
                        });
                    });
                });
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            });
        </script>
        <link rel="stylesheet" type="text/css" href="displayProductStyle.css">
        <link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400" rel="stylesheet" type="text/css">
        <title>Display Product</title>
    </head>

    <body>
        <header class="clearfix masthead">

            <div id="nav1">
                <ul>
                    <li><a href="contactus.jsp" class="topMenu">Contact Us</a></li>
                    <li><a href="ConfirmAdd.jsp"
                           class="topMenu">Order Status</a></li>
                    <li><a href="Compare.jsp"
                           class="topMenu">Compare</a></li>

                </ul>
            </div>
            <a href="browseAllProducts.jsp"><h1
                    class="compName">CameraWorld.com</h1></a> 
        </header>
        <br>
        <hr>
        <div id="nav2">
            <ul class="ulmenu">
                <li id="all"><a href="browseAllProducts.jsp" class = "topMenu">All</a></li>
                <li id="nikon"><a href="#" class = "topMenu">Nikon</a></li>
                <li id="canon"><a href="#" class = "topMenu">Canon</a></li>
                <li id="sony"><a href="#" class = "topMenu">Sony</a></li>
                <li id="olympus"><a href="#" class = "topMenu">Olympus</a></li>
            </ul>
        </div>
        <div id="productGrid" class="productGrid" style="margin-left: 300px">
            <div id=imageDetails style= "display:inline-block; vertical-align:top"></div>
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                <a class="addthis_button_tweet"></a>
                <a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
            </div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-534c01e715ebe8c2"></script>
            <!-- AddThis Button END -->
            <div id="productDetails" class="productDetails" style="display:inline-block"></div>
            <div id="tableDetails" class="tableDetails"></div>
            <div id="reviews"></div>
            <div id="fb-root"></div>
        </div>
        <!-- Go to http://www.addthis.com/get/smart-layers to customize -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-534c01e715ebe8c2"></script>
        <script type="text/javascript">
            addthis.layers({
                'theme': 'transparent',
                'share': {
                    'position': 'right',
                    'numPreferredServices': 6
                }
            });
        </script>
    </body>
</html>