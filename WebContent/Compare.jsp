<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
	<%@ page import="java.sql.*;" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.alerts.css"  rel="stylesheet" type="text/css" media="screen"/>
<script>
var getProducts = function(productBrand){
	var url = "BrowseAllProducts?Product="+productBrand;
	$.get(url,function(data,status){	
	var productObj= $.parseJSON(data);
		var dropdown = document.getElementById("ddl1");
		$.each(productObj, function(idx, obj){
		var opt = document.createElement("option"); 
		opt.text = obj.productName;
		opt.value = obj.productID;
		dropdown.options.add(opt);
		});
		
		var dropdown = document.getElementById("ddl2");
		$.each(productObj, function(idx, obj){
		var opt = document.createElement("option"); 
		opt.text = obj.productName;
		opt.value = obj.productID;
		dropdown.options.add(opt);
		});

	});
	};
		
	$(document).ready(function(){
		getProducts("all");
		$(".myButton").click( function()
				{
		 	var ddlval1 = $("#ddl1").find('option:selected').val();
			var ddlval2 = $("#ddl2").find('option:selected').val();  
			if(ddlval1==ddlval2)
				{				 
	                    $('#overlay').fadeIn('fast',function(){
	                        $('#box').animate({'top':'1px'},500);
	                    });
	                    $('#close').click(function(){
	                        $('#box').animate({'top':'-200px'},500,function(){
	                            $('#overlay').fadeOut('fast');
	                        });
	                    });

				}
				//alert("Select two different cameras");	
			else
			window.location.assign("ShowCompare.jsp?productID="+ddlval1+"&productID="+ddlval2); 

				});

			
		});
</script>

<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link rel="stylesheet" type="text/css" href="Compare.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="displayProductStyle.css">


<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
 <!-- Enable responsive view on mobile devices -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Compare Cameras</title>
</head>
<body>


	<header class="clearfix masthead">
		
		<div id="nav1">
		<ul>
			<li><a href="contactus.jsp" class = "topMenu">Contact Us</a></li>
			<li><a href="ConfirmAdd.jsp"
				class="topMenu">Order Status</a></li>
			<li><a href="Compare.jsp" class = "topMenu">Compare</a></li>
			<li><a href="logout.jsp" class = "topMenu">LogOut</a></li>
			
		</ul>
	</div>
	<a href="browseAllProducts.jsp"><h1
			class="compName">CameraWorld.com</h1></a> 
	</header>
	
	
	<br>
	<hr>
	<div id="nav2">
		<ul class="ulmenu">
			<li id="all"><a href="browseAllProducts.jsp" class = "topMenu">All</a></li>
			<li id="nikon"><a href="#"  class = "topMenu">Nikon</a></li>
			<li id="canon"><a href="#" class = "topMenu">Canon</a></li>
			<li id="sony"><a href="#" class = "topMenu">Sony</a></li>
			<li id="olympus"><a href="#" class = "topMenu">Olympus</a></li>
		</ul>
	</div>
	
	<div id="productGrid" class="productGrid" style="margin-left: 300px;">
	<div style="font:Raleway; font-size: 30px;"> 
	<p> CAMERAS TO COMPARE </p>
</div>
<div style="margin-left:300px;">
<div style="font:Raleway; font-size: 20px;"> 
Camera 1:
</div>
	<div class = "styled-select"> 
	<select id="ddl1">
	</select>
	</div> 
	<p> </p>
	<div style="font:Raleway; font-size: 20px;"> 
Camera 2:
</div>
	<div class = "styled-select">
	<select id="ddl2">
	</select>	
	</div>
	<p>	</p>
	<div class = "myButton">
	<!-- <button type="button" name="addToCompare"  value="Add To Compare" style="font-size: 20px; height: 40px; width: 200px; margin-left: 5px; margin-top:20px;"> -->
	Compare
	<!-- </button> -->
	</div>
	</div>
	</div>
		
	<div class="overlay" id="overlay" style="display:none;"></div>
<div class="box" id="box">
 <a class="boxclose" id="boxclose"></a>
 <h1 style="font:Raleway; font-size: 25px;"> Hi there!</h1>
 <p style="font:Raleway; font-size: 20px;">
 Please choose different cameras.
 </p>
 <p></p>
 <div id="close" style="font:Raleway; font-size: 20px; color:blue; cursor:pointer"> Click to Close </div>
</div>
</body>
</html>