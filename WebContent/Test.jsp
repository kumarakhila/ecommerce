<%@page import="com.eshopping.items.ProductDetails"%>
<%@page import="com.eshopping.items.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.eshopping.database.ProductDBAO"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Insert title here</title>
</head>
<body>
	<%
		ProductDBAO p = new ProductDBAO();
		ProductDetails pro = p.getProductDetails(1);

		System.out.println(pro.getDimension().getProductID()
				+ pro.getDimension().getWeight()
				+ pro.getDimension().getDimension());
		System.out.println(pro.getGeneral().getProductID()
				+ pro.getGeneral().getType() + pro.getGeneral().getColor()
				+ pro.getGeneral().getModelID());
		System.out.println(pro.getImage().getProductID()
				+ pro.getImage().getImageSizing()
				+ pro.getImage().getImageFormat()
				+ pro.getImage().getVideoFormat()
				+ pro.getImage().getFaceDetection()
				+ pro.getImage().getShottingModes());
		System.out.println(pro.getLens().getAutoFocus()
				+ pro.getLens().getFocalLength()
				+ pro.getLens().getManualFocus());
		
	%>
</body>
</html>