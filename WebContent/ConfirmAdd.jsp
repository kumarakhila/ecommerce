<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
#continue {
	font-size: 20px;
	font-family: 'Raleway', sans-serif;
	height: 50px;
	width: 250px;
	margin-left: 235px;
	margin-top: 20px;
}

#checkout {
	font-size: 20px;
	font-family: 'Raleway', sans-serif;
	height: 50px;
	width: 250px;
	margin-left: 10px;
	margin-top: 20px;
}

#productDetails {
	margin-left: 50px;
	margin-top: 15px;
	font-size: 18px;
	width: 700px;
}

#shoppingCartHeading {
	margin-left: 50px;
	margin-top: 150px;
}

th {
	border: 3px solid black;
}
</style>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
	$(document).ready(
			function() {

				$.get("DisplayCart/",
						function(data, status) {
							var productObj = $.parseJSON(data);

							var tableBody = $("<tbody></tbody>");
							$('#productDetails').append(tableBody);
							var count =0;
							$.each(productObj, function(idx, obj) {
								count++;
								tableBody.append("<tr><td>" + obj.productName
										+ "</td><td>" + obj.price + "</tr>");

							});
							if(count==0)
							{
								$("#shoppingCartHeading").text("No products in the cart");
								$("#productDetails").hide();
								$("#checkout").hide();
								
								
							}

						});
			});
	function gotoBrowseAll() {
		window.location = 'browseAllProducts.jsp';
	}
	
	function gotoCheckOut() {
		$.get("LoginStatus/",
				function(data, status) {
			var sessionStatus=$.parseJSON(data);
			console.log(sessionStatus);
			if(sessionStatus){
				window.location = 'success.jsp';
			}else{
				window.location = 'front.jsp';
			}
		});
		
	}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
</head>

<body>

	<header class="clearfix masthead">

	<div id="nav1">
		<ul>
			<li><a href="#" class="topMenu">Contact Us</a></li>
			<li><a href="#" class="topMenu">Order Status</a></li>
			<li><a href="Compare.jsp"
				class="topMenu">Compare</a></li>

		</ul>
	</div>
	<a href="browseAllProducts.jsp"><h1
			class="compName">CameraWorld.com</h1></a> 
	</header>

	<br>
	<h1 id="shoppingCartHeading">Products in shopping cart</h1>

	<table id="productDetails" class="pure-table pure-table-bordered">
		<thead>
			<tr>
				<th>Product Name</th>
				<th width="200">Product Price</th>
			</tr>
		</thead>

	</table>
	<button class="pure-button" name="continue" id="continue"
		onclick="gotoBrowseAll()">Continue Shopping</button>
	<button class="pure-button  pure-button-primary" type="button" name="checkout" id="checkout"
		onclick="gotoCheckOut()">Check Out Cart</button>

</body>
</html>