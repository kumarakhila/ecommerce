<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page
	import="com.google.gson.reflect.TypeToken,com.google.gson.Gson,com.eshopping.items.Product"%>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
	var getProducts = function(productBrand) {
		var url = "BrowseAllProducts?Product="
				+ productBrand;
		$.get(url, function(data, status) {
			var productObj = $.parseJSON(data);
			var count = 0;
			var divBox = 0;
			$.each(productObj, function(idx, obj) {
				if (count % 4 == 0) {
					divBox = $("<div></div>").attr('class', "grid grid-pad");
					$("#productGrid").append(divBox);
				}
				var divInner = $("<div></div>").attr('class',
						"col-1-4 icon-box");
				var divContent = $("<div></div>").attr('class', "content");
				var image = $("<img></img>").attr('src', obj.imageLink).css({
					"width" : "200px",
					"height" : "150px"
				});
				image.attr('class', "imageLink");
				image.attr('alt', "Responsive");
				var anchor = $("<a></a>").attr(
						'href',
						"DisplayProduct.jsp?productID="
								+ obj.productID);
				anchor.append(image);
				var productName = $("<h2></h2>").text(obj.productName);
				var productPrice = $("<h3></h3>").text("$ " + obj.price);
				var rating = $("<img></img>").attr('src', obj.rating).css({
					"height" : "30px",
					"width" : "85px"
				});
				count++;
				/* var divTitleBox= $("<div></div>").attr('class',"titleBox");
				
				if(obj.quantity>0)
				{
				divTitleBox.text("Available");
				}
				else
				{
				divTitleBox.text("Not Available");
				}  */

				divContent.append(anchor, productName, productPrice, rating);
				divInner.append(divContent);
				divBox.append(divInner);
				/* if((count%4)==0){
					var after=$("::after");
					divInner.append(after);
				} */

			});
		});
	};
	$(document).ready(function() {

		getProducts("all");
		$("ul.ulmenu li").click(function(e) {
			$('#productGrid').empty();
			getProducts(this.id);

		});

	});
</script>

<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400"
	rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<!-- Enable responsive view on mobile devices -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Browse All the Cameras</title>
</head>
<body>

	<header class="clearfix masthead">

	<div id="nav1">
		<ul>
			<li><a href="contactus.jsp" class="topMenu">Contact Us</a></li>
			<li><a href="ConfirmAdd.jsp"
				class="topMenu">Order Status</a></li>
			<li><a href="Compare.jsp"
				class="topMenu">Compare</a></li>
		</ul>
	</div>
	<a href="browseAllProducts.jsp"><h1
			class="compName">CameraWorld.com</h1></a> 
	</header>


	<br>
	<hr>
	<div id="nav2">
		<ul class="ulmenu">
			<li id="all"><a
				href="browseAllProducts.jsp"
				class="topMenu">All</a></li>
			<li id="nikon"><a href="#" class="topMenu">Nikon</a></li>
			<li id="canon"><a href="#" class="topMenu">Canon</a></li>
			<li id="sony"><a href="#" class="topMenu">Sony</a></li>
			<li id="olympus"><a href="#" class="topMenu">Olympus</a></li>
		</ul>
	</div>



	<div id="productGrid" class="productGrid"></div>



	<!-- <div style="width: .25em; height: 5em; background: #f00;"></div> -->


</body>
</html>