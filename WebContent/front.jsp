<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
<title>Login Page</title>
<style>
.pure-button {
	width: 150px;
	margin-left: 270px;
}

#registerPrompt{
	margin-left: 200px;
}
</style>
</head>
<body>
	<form method="post" action="login.jsp"
		class="pure-form pure-form-aligned">
		<br> <br> <br> <br> <br> <br> <br>
		<br> <br>
		<center>
			<form class="pure-form pure-form-aligned">
				<fieldset>
					<div class="pure-control-group">
						<label for="name">Username</label> <input name="uname" type="text"
							placeholder="Username">
					</div>

					<div class="pure-control-group">
						<label for="password">Password</label> <input name="pass"
							type="password" placeholder="Password">
					</div>

					<br>
					<button type="submit" class="pure-button pure-button-primary"
						width="150">Login</button>
					<br> <br>

					<button type="reset" class="pure-button pure-button-primary">Reset</button>
					<div>
						<p id="registerPrompt">Not Registered? <a href="reg.jsp">Register Here</a> </p> 
					</div>
				</fieldset>
		</center>
	</form>
</body>
</html>
<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400"
	rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<!-- Enable responsive view on mobile devices -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Browse All the Cameras</title>
</head>
<body>

	<header class="clearfix masthead">

		<div id="nav1">
			<ul>
				<li><a href="#" class="topMenu">Contact Us</a></li>
				<li><a href="http://localhost:8080/e_shopping/ConfirmAdd.jsp"
				class="topMenu">Order Status</a></li>
				<li><a href="http://localhost:8080/e_shopping/Compare.jsp"
				class="topMenu">Compare</a></li>
			</ul>
		</div>
		<a href="http://localhost:8080/e_shopping/browseAllProducts.jsp"><h1
			class="compName">CameraWorld.com</h1></a> 
	</header>


	<br>

	<div id="nav2">
		<ul class="ulmenu">
			<li id="all"><a
				href="http://localhost:8080/e_shopping/browseAllProducts.jsp"
				class="topMenu">All</a></li>
			<li id="nikon"><a href="#" class="topMenu">Nikon</a></li>
			<li id="canon"><a href="#" class="topMenu">Canon</a></li>
			<li id="sony"><a href="#" class="topMenu">Sony</a></li>
			<li id="olympus"><a href="#" class="topMenu">Olympus</a></li>
		</ul>
	</div>
</body>
