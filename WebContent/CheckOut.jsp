<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">

#productDetails {
	margin-left: 50px;
	margin-top: 10px;
	font-size: 18px;
}

#thankYouTitle {
	margin-top: 200px;
	margin-left: 50px;
}

#shoppingItemsTitle {
	margin-left: 50px;
}
</style>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
	$(document).ready(
			function() {

				$.get("DisplayCart/?clear=1",
						function(data, status) {
							var productObj = $.parseJSON(data);
							var total = 0;
							
							var tableBody = $ ("<tbody></tbody>");
							$('#productDetails').append(tableBody);
							
							$.each(productObj, function(idx, obj) {
								tableBody.append(
										"<tr><td>" + obj.productName
												+ "</td><td>" + obj.price
												+ "</tr>");
								total = total + obj.price;

							});
							tableBody.append(
									"<tr><td>" + "Total" + "</td><td>" + total
											+ "</td></tr>");

						});
			});
	function gotoBrowseAll() {
		window.location = 'browseAllProducts.jsp';
	}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
</head>

<body>

	<header class="clearfix masthead">

	<div id="nav1">
		<ul>
			<li><a href="contactus.jsp" class="topMenu">Contact Us</a></li>
			<li><a href="ConfirmAdd.jsp"
				class="topMenu">Order Status</a></li>
			<li><a href="Compare.jsp"
				class="topMenu">Compare</a></li>
			<li><a href="logout.jsp" class="topMenu">LogOut</a></li>

		</ul>
	</div>
	<a href="browseAllProducts.jsp"><h1
			class="compName">CameraWorld.com</h1></a> 
	</header>
	<h1 id="thankYouTitle">Thank you for shopping with us!!</h1>
	<h1 id="shoppingItemsTitle">You have purchased the below items!!</h1>
	<br>
	<br>
	<br>

	<table id="productDetails" class="pure-table pure-table-bordered">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Product Price</th>
		</tr>
	</thead>
	</table>


</body>
</html>