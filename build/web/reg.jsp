<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link rel="stylesheet"
	href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
<style>
.pure-button {
	width: 150px;
	margin-left: 270px;
}

#alreadyRegisteredPrompt {
	margin-left: 200px;
}
</style>
</head>
<body>
	<form method="post" action="registration.jsp"
		class="pure-form pure-form-aligned">
		<center>
			<br> <br> <br> <br> <br> <br> <br>
			<fieldset>
				<div class="pure-control-group">
					<label for="name">First Name</label> <input name="fname"
						type="text" placeholder="First Name">
				</div>

				<div class="pure-control-group">
					<label for="name">Last Name</label> <input name="lname" type="text"
						placeholder="Last Name">
				</div>

				<div class="pure-control-group">
					<label for="email">Email Address</label> <input id="email"
						type="email" placeholder="Email Address">
				</div>

				<div class="pure-control-group">
					<label for="name">User Name</label> <input name="uname" type="text"
						placeholder="User Name">
				</div>

				<div class="pure-control-group">
					<label for="password">Password</label> <input name="pass"
						type="password" placeholder="Password">
				</div>



				<button type="submit" class="pure-button pure-button-primary">Register</button>
				<br> <br>

				<button type="reset" class="pure-button pure-button-primary">Reset</button>

				<p id="alreadyRegisteredPrompt">
					Already registered? <a href="front.jsp">Login Here</a>
				</p>
			</fieldset>
		</center>
	</form>
</body>
</html>
<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400"
	rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<!-- Enable responsive view on mobile devices -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Browse All the Cameras</title>
</head>
<body>

	<header class="clearfix masthead">

		<div id="nav1">
			<ul>
				<li><a href="contactus.jsp" class="topMenu">Contact Us</a></li>
				<li><a href="ConfirmAdd.jsp"
				class="topMenu">Order Status</a></li>
				<li><a href="Compare.jsp"
					class="topMenu">Compare</a></li>
			</ul>
		</div>
		<a href="browseAllProducts.jsp"><h1
				class="compName">CameraWorld.com</h1></a>
	</header>


	<br>

	<div id="nav2">
		<ul class="ulmenu">
			<li id="all"><a
				href="browseAllProducts.jsp"
				class="topMenu">All</a></li>
			<li id="nikon"><a href="#" class="topMenu">Nikon</a></li>
			<li id="canon"><a href="#" class="topMenu">Canon</a></li>
			<li id="sony"><a href="#" class="topMenu">Sony</a></li>
			<li id="olympus"><a href="#" class="topMenu">Olympus</a></li>
		</ul>
	</div>
</body>