<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="jquery.raty.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#rating').raty({
                    path: 'img',
                    click: function(score, evt) {
                        $("input[name='rating']").val(score);
                    }
                });
            });
        </script>
        <style>
            #rating, #rating img {display: inline;}
        </style>
        <title>Comment Page</title>
    </head>
    <body><br><br><br>
        <br><br><br>
        <form method="post" action="browseAllProducts.jsp">
            <font face="georgia" >
                <center><br><br><B><h1>Write a CameraWorld Review</h1></B><br>
                    '<font color='red'>*</font>'<font color='grey' size="2"> denoted are mandatory fields</font>

                    <table border="1" width="50%" cellpadding="5">
                        <tbody bgcolor="lightgrey">
                            <tr><td><b>
                                        Your Reviews<font color='red'>*</font>:</b> &nbsp;&nbsp;&nbsp;<input type="text" name="review" value="" style="width:500px; height:250px;"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='grey' size="2">(Maximum 250 characters)</font></td>
                            </tr></td>
                            <tr><td>
                                    <b>Your Rating:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                    <div id="rating"></div>
                                    <input type="hidden" name="rating" value="" style="width:100px; height:30px;" />    &nbsp;&nbsp;&nbsp;&nbsp;<font color='grey' size="2">(Hint:Provide rating ranging from scale of 1-5 numbers)</font>
                            </tr></td>
                            <tr><td><b>
                                        Your Name<font color='red'>*</font>:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="text" name="name" value="" style="width:200px; height:30px;"/>
                            </tr></td>
                            <tr>  <td align="center">
                                    <input type="submit" value="Submit" onclick="alert('your reviews are submitted !');" style="width:60px; height:30px;" />
                                    <input type="reset" value="Reset" style="width:60px; height:30px;" />
                            </tr></td><td>
                            <font color='grey'><B>Had a great experience buying on Flipkart Or do you think we can do better? Tell us by reviewing</B></font>
                        </td>
                        </tbody>
                    </table>
                </center>
            </font>
        </form>
    </body>
</html>
<link rel="stylesheet" type="text/css" href="browseProductsStyle.css">
<link href="http://fonts.googleapis.com/css?family=Raleway:200,100,400" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<!-- Enable responsive view on mobile devices -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Browse All the Cameras</title>
</head>
<body>

<header class="clearfix masthead">

    <div id="nav1">
        <ul>
            <li><a href="contactus.jsp" class = "topMenu">Contact Us</a></li>
            <li><a href="#" class = "topMenu">Order Status</a></li>
            <li><a href="front.jsp" class = "topMenu">LogOut</a></li>
            <li><a href="#" class = "topMenu">Sign Up</a></li>
        </ul>
    </div>
    <h1 class="compName">CameraWorld.com</h1>
</header>
</body>